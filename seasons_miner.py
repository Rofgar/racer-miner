import requests
import os.path
import codecs
import json
from json import JSONDecoder, JSONEncoder
from os import path
from lxml import html
from lxml.cssselect import CSSSelector
from itertools import groupby
from datetime import datetime


class ObjectEncoder(JSONEncoder):
    def default(self, obj):
        return obj.__dict__


class SetEncoder(json.JSONEncoder):
    def default(self, obj):
       if isinstance(obj, set):
          return list(obj)
       return json.JSONEncoder.default(self, obj)


class Season():
    def __init__(self, id, name, classes = []):
        self.id = id
        self.name = name
        self.classes = classes

    def add_class(self, season_class):
        self.classes.append(season_class)

    def __repr__(self):
        return f'{self.name} [{self.id}]'


class SeasonClass():
    def __init__(self, id, name):
        self.id = id
        self.name = name

    def __repr__(self):
        return f'{self.name} [{self.id}]'


class SeasonDecoder(JSONDecoder):
    def __init__(self):
        json.JSONDecoder.__init__(self, object_hook=self.dict_to_object)

    def dict_to_object(self, dictionary):
        if (dictionary.get('classes')):
            return Season(id=dictionary['id'], name=dictionary['name'], classes=dictionary['classes'])
        else:
            return SeasonClass(id=dictionary['id'], name=dictionary['name'])


class SeasonPoints():
    def __init__(self, season_id, season_name, season_class, points):
        self.season_id = season_id
        self.season_name = season_name
        self.season_class = season_class
        self.points = points
    def __repr__(self):
        return f'{self.season_name} [{self.season_id}] {self.season_class}: {self.points}'


class SeasonPointsEntry():
    def __init__(self, position, surname, name, motorcycle, points):
        self.position = position
        self.surname = surname
        self.name = name
        self.motorcycle = motorcycle
        self.points = points
    def __repr__(self):
        return f'#{self.position} {self.surname} {self.name} [{self.points}]'


class SeasonStage():
    def __init__(self, season_id, season_name, season_class, id, name):
        self.season_id = season_id
        self.season_name = season_name
        self.season_class = season_class
        self.id = id
        self.name = name
    def __repr__(self):
        return f'[{self.season_name}] {self.season_class.name} {self.name}'


class SeasonStageDecoder(JSONDecoder):
    def __init__(self):
        json.JSONDecoder.__init__(self, object_hook=self.dict_to_object)

    def dict_to_object(self, dictionary):
        if (dictionary.get('season_class')):
            return SeasonStage(
                season_id=dictionary['season_id'],
                season_name=dictionary['season_name'],
                season_class=dictionary['season_class'],
                id=dictionary['id'], 
                name=dictionary['name']
            )
        else:
            return SeasonClass(
                id=dictionary['id'],
                name=dictionary['name']
            )


class StageSession():
    def __init__(self, season_id, season_name, season_class, season_stage, id, name):
        self.season_id = season_id
        self.season_name = season_name
        self.season_class = season_class
        self.season_stage = season_stage
        self.id = id
        self.name = name
    def __repr__(self):
        return f'[{self.season_name}] {self.season_class.name} {self.season_stage.name} {self.name}'


class StageSessionDecoder(JSONDecoder):
    def __init__(self):
        json.JSONDecoder.__init__(self, object_hook=self.dict_to_object)

    def dict_to_object(self, dictionary):
        if (dictionary.get('season_stage')):
            return StageSession(
                season_id=dictionary['season_id'],
                season_name=dictionary['season_name'],
                season_class=dictionary['season_class'],
                season_stage=dictionary['season_stage'],
                id=dictionary['id'],
                name=dictionary['name']
            )
        elif (dictionary.get('season_class')):
            return SeasonStage(
                season_id=dictionary['season_id'],
                season_name=dictionary['season_name'],
                season_class=dictionary['season_class'],
                id=dictionary['id'], 
                name=dictionary['name']
            )
        else:
            return SeasonClass(
                id=dictionary['id'],
                name=dictionary['name']
            )


class SessionEntry():
    def __init__(self, position, surname, name, motorcycle, laps, total_time, best_lap_time, diff, points = None):
        self.position = position
        self.surname = surname
        self.name = name
        self.motorcycle = motorcycle
        self.points = points
        self.laps = laps
        self.total_time = total_time
        self.best_lap_time = best_lap_time
        self.diff = diff
    def __repr__(self):
        return f'[{self.position}] {self.surname} {self.name} [{self.best_lap_time}] {self.points} {self.diff} [{self.laps}]'


class StageSessionWithEntries():
    def __init__(self, stage_session, session_entries):
        self.stage_session = stage_session
        self.session_entries = session_entries
    def __repr__(self):
        return f'{self.stage_session} {self.session_entries}'


class StageSessionWithEntriesDecoder(JSONDecoder):
    def __init__(self):
        json.JSONDecoder.__init__(self, object_hook=self.dict_to_object)

    def dict_to_object(self, dictionary):
        if (dictionary.get('stage_session')):
            return StageSessionWithEntries(
                stage_session = dictionary['stage_session'],
                session_entries = dictionary['session_entries']
            )
        elif (dictionary.get('season_stage')):
            return StageSession(
                season_id=dictionary['season_id'],
                season_name=dictionary['season_name'],
                season_class=dictionary['season_class'],
                season_stage=dictionary['season_stage'],
                id=dictionary['id'],
                name=dictionary['name']
            )
        elif (dictionary.get('season_class')):
            return SeasonStage(
                season_id=dictionary['season_id'],
                season_name=dictionary['season_name'],
                season_class=dictionary['season_class'],
                id=dictionary['id'], 
                name=dictionary['name']
            )
        elif (dictionary.get('position')):
            return SessionEntry(
                position = dictionary['position'],
                surname = dictionary['surname'],
                name = dictionary['name'],
                motorcycle = dictionary['motorcycle'],
                points = dictionary['points'],
                laps = dictionary['laps'],
                total_time = dictionary['total_time'],
                best_lap_time = dictionary['best_lap_time'],
                diff = dictionary['diff']
            )
        else:
            return SeasonClass(
                id=dictionary['id'],
                name=dictionary['name']
            )


def collect_seasons():
    data = {
        'controller': 'Statistics', 
        'method': 'getStatisticsCategory'
    }

    seasons_response = requests.post('http://racer.com.ua/ua/statistics/rezultaty-i-statistika', data = data)
    if (not seasons_response.ok):
        print('Could not retrieve seasons list')
        return

    seasons_html = html.fromstring(seasons_response.text)

    seasons = []
    for season in seasons_html.cssselect('.season-block option'):
        seasons.append(Season(season.get('value'), season.text))

    return seasons

def collect_season_classes(seasons):
    all_unique_classes = set()
    for season in seasons:
        headers = {
            'X-Requested-With': 'XMLHttpRequest',
            'User-Agent': 'Big bold schlong from BraZZers'
        }
        
        data = {
            'controller': 'Statistics', 
            'method': 'getStatisticsCategory',
            'seasonId': season.id
        }

        classes_response = requests.post(
            'http://racer.com.ua/ua/statistics/rezultaty-i-statistika',
            headers = headers,
            data = data
        )
        if (not classes_response.ok):
            print(f'Could not retrieve classes for season {season}')
            return

        classes_html = html.fromstring(json.loads(classes_response.text).get('html'))

        for li_season_class in classes_html.cssselect('ol li'):
            id = json.loads(li_season_class.get('data-ajax-params'))['categoryId']
            sanitized_class = li_season_class.cssselect('span')[0].text.strip()
            all_unique_classes.add(sanitized_class)
            season.add_class(SeasonClass(id=id, name=sanitized_class))
    
    cache_seasons(seasons)
    cache_class_names(all_unique_classes)

    return seasons, all_unique_classes


def cache_seasons(seasons):
    try:
        with codecs.open('seasons.dump', 'wb') as seasons_file:
            seasons_file.write(json.dumps(seasons, cls=ObjectEncoder, ensure_ascii=False).encode('utf8'))
    except Exception as e:
        print('Failed to write seasons to cache: ' + str(e))
        os.remove('seasons.dump')


def read_seasons_cache():
    with codecs.open('seasons.dump', 'rb') as seasons_file:
        seasons = json.loads(seasons_file.read(), cls=SeasonDecoder)
        return seasons


def cache_class_names(class_names):
    try:
        with codecs.open('class_names.dump', 'wb') as seasons_file:
            seasons_file.write(json.dumps(class_names, cls=SetEncoder, ensure_ascii=False).encode('utf8'))
    except Exception as e:
        print('Failed to write classes to cache: ' + str(e))
        os.remove('class_names.dump')


def read_class_names_cache():
    with codecs.open('class_names.dump', 'rb') as seasons_file:
        seasons = json.loads(seasons_file.read())
        return seasons


def collect_season_points(seasons):
    season_points = []
    for season in seasons:
        for season_class in season.classes:
            headers = {
                'X-Requested-With': 'XMLHttpRequest',
                'User-Agent': 'Big bold schlong from BraZZers'
            }

            data = {
                'controller': 'Statistics',
                'method': 'getStatistics',
                'seasonId': season.id,
                'categoryId': season_class.id
            }

            season_class_points_response = requests.post(
                'http://racer.com.ua/ua/statistics/rezultaty-i-statistika',
                headers = headers,
                data = data
            )
            if (not season_class_points_response.ok):
                print(f'Could not retrieve points for season class {season_class}')
                return

            season_class_points_html = html.fromstring(json.loads(season_class_points_response.text).get('html'))

            season_points_entries = []
            for row in season_class_points_html.cssselect('table.statistic-table tr'):
                columns = row.cssselect('td')
                if (columns):
                    season_points_entries.append(
                        SeasonPointsEntry(
                            position = columns[0].text,
                            surname = columns[1].cssselect('a')[0].text,
                            name = columns[2].cssselect('a')[0].text,
                            motorcycle = columns[3].text,
                            points = columns[4].text
                        )
                    )
            season_points.append(SeasonPoints(season.id, season.name, season_class, season_points_entries))

    cache_season_points(season_points)
    return season_points


def cache_season_points(season_points):
    try:
        with codecs.open('seasons_points.dump', 'wb') as seasons_file:
            seasons_file.write(json.dumps(season_points, cls=ObjectEncoder, ensure_ascii=False).encode('utf8'))
    except Exception as e:
        print('Failed to write seasons points to cache: ' + str(e))
        os.remove('seasons_points.dump')


def read_season_points_cache():
    with codecs.open('seasons_points.dump', 'rb') as seasons_file:
        seasons_points = json.loads(seasons_file.read())
        return seasons_points


def collect_season_stages(seasons_with_classes):
    season_stages = []
    for season in seasons_with_classes:
        for season_class in season.classes:
            headers = {
                'X-Requested-With': 'XMLHttpRequest',
                'User-Agent': 'Fake Agent - Ava Adams'
            }

            data = {
                'controller': 'Statistics',
                'method': 'getStatisticsEvent',
                'categoryId': season_class.id,
                'seasonId': season.id
            }

            season_stages_response = requests.post(
                'http://racer.com.ua/ua/statistics/rezultaty-i-statistika',
                headers = headers,
                data = data
            )
            if (not season_stages_response.ok):
                print(f'Could not retrieve points for season class {season_class}')
                return

            season_stages_html = html.fromstring(json.loads(season_stages_response.text).get('html'))
            for season_stage_li in season_stages_html.cssselect('ol.list-events li'):
                id = json.loads(season_stage_li.get('data-ajax-params'))['eventId']
                sanitized_stage_name = season_stage_li.cssselect('span')[0].text.strip()
                season_stages.append(SeasonStage(season.id, season.name, season_class, id, sanitized_stage_name))

    cache_season_stages(season_stages)
    return season_stages


def cache_season_stages(season_stages):
    try:
        with codecs.open('season_stages.dump', 'wb') as season_stages_file:
            season_stages_file.write(json.dumps(season_stages, cls=ObjectEncoder, ensure_ascii=False).encode('utf8'))
    except Exception as e:
        print('Failed to write seasons stages to cache: ' + str(e))
        os.remove('season_stages.dump')


def read_season_stages_cache():
    with codecs.open('season_stages.dump', 'rb') as season_stages_file:
        return json.loads(season_stages_file.read(), cls=SeasonStageDecoder)


def collect_stage_sessions(season_stages):
    stage_sessions = [] 
    for season_stage in season_stages:
        headers = {
            'X-Requested-With': 'XMLHttpRequest',
            'User-Agent': 'Bugs Bunny'
        }

        data = { 
            'eventId': season_stage.id,
            'controller': 'Statistics',
            'method': 'getStatisticsSession'
        }

        sessions_response = requests.post(
            'http://racer.com.ua/ua/statistics/rezultaty-i-statistika',
            headers = headers,
            data = data
        )
        if (not sessions_response.ok):
            print(f'Could not retrieve sessions for stage {season_stage}')
            return

        sessions_html = html.fromstring(json.loads(sessions_response.text).get('html'))
        for session_li in sessions_html.cssselect('ol.list-category li'):
            id = session_li.get('data-id')
            name = session_li.cssselect('span')[0].text.strip()
            stage_sessions.append(StageSession(
                season_id = season_stage.season_id, 
                season_name = season_stage.season_name,
                season_class = season_stage.season_class,
                season_stage = season_stage,
                id = id,
                name = name
            ))

    cache_stage_sessions(stage_sessions)
    return stage_sessions


def cache_stage_sessions(stage_sessions):
    try:
        with codecs.open('stage_sessions.dump', 'wb') as stage_sessions_file:
            stage_sessions_file.write(json.dumps(stage_sessions, cls=ObjectEncoder, ensure_ascii=False).encode('utf8'))
    except Exception as e:
        print('Failed to write stage sessions to cache: ' + str(e))
        os.remove('stage_sessions.dump')


def read_stage_sessions_cache():
    with codecs.open('stage_sessions.dump', 'rb') as stage_sessions_file:
        return json.loads(stage_sessions_file.read(), cls=StageSessionDecoder)


def collect_session_entries(stage_sessions):
    stage_sessions_with_entries = []

    for stage_session in stage_sessions:
        headers = {
            'X-Requested-With': 'XMLHttpRequest',
            'User-Agent': 'Lola Bunny'
        }

        data = {
            'controller': 'Statistics',
            'method': 'getStatistics',
            "seasonId": stage_session.season_id,
            "categoryId": stage_session.season_class.id,
            "eventId": stage_session.season_stage.id,
            "sessionId": stage_session.id
        }

        session_entries_response = requests.post(
            'http://racer.com.ua/ua/statistics/rezultaty-i-statistika',
            headers = headers,
            data = data
        )
        if (not session_entries_response.ok):
            print(f'Could not retrieve session entries for {stage_session}')
            return

        session_entries_html = html.fromstring(json.loads(session_entries_response.text).get('html'))

        session_entries = []
        for session_entry_row in session_entries_html.cssselect('table.statistic-table tbody tr'):
            columns = session_entry_row.cssselect('td')
            if (stage_session.name == 'Q' and columns and len(columns) == 8):
                session_entries.append(
                    SessionEntry(
                        position = columns[0].text,
                        surname = columns[1].cssselect('a')[0].text,
                        name = columns[2].cssselect('a')[0].text,
                        motorcycle = columns[3].text,
                        laps = columns[4].text,
                        total_time = columns[5].text,
                        best_lap_time = columns[6].text,
                        diff = columns[7].text,
                    )
                )
            elif (columns and len(columns) == 9):
                session_entries.append(
                    SessionEntry(
                        position = columns[0].text,
                        surname = columns[1].cssselect('a')[0].text,
                        name = columns[2].cssselect('a')[0].text,
                        motorcycle = columns[3].text,
                        points = columns[4].text,
                        laps = columns[5].text,
                        total_time = columns[6].text,
                        best_lap_time = columns[7].text,
                        diff = columns[8].text,
                    )
                )
        stage_sessions_with_entries.append(StageSessionWithEntries(stage_session, session_entries))

    cache_session_entries(stage_sessions_with_entries)
    return stage_sessions_with_entries


def cache_session_entries(session_entries):
    try:
        with codecs.open('session_entries.dump', 'wb') as session_entries_file:
            session_entries_file.write(json.dumps(session_entries, cls=ObjectEncoder, ensure_ascii=False).encode('utf8'))
    except Exception as e:
        print('Failed to write session entries to cache: ' + str(e))
        os.remove('session_entries.dump')


def read_session_entries_cache():
    with codecs.open('session_entries.dump', 'rb') as session_entries_file:
        return json.loads(session_entries_file.read(), cls=StageSessionWithEntriesDecoder)


def mine():
    seasons_with_classes = []
    all_unique_classes = []
    if (path.exists('seasons.dump') and path.exists('class_names.dump')):
        all_unique_classes = read_class_names_cache()
        seasons_with_classes = read_seasons_cache()        
    else:
        seasons_with_classes, all_unique_classes = collect_season_classes(
            collect_seasons()
        )
    print(f'Collected {len(seasons_with_classes)} seasons with {len(all_unique_classes)} classes')

    season_points = []
    if (path.exists('seasons_points.dump')):
        season_points = read_season_points_cache()
    else:
        season_points = collect_season_points(seasons_with_classes)
    print(f'Collected {len(season_points)} season points')

    season_stages = []
    if (path.exists('season_stages.dump')):
        season_stages = read_season_stages_cache()
    else:
        season_stages = collect_season_stages(seasons_with_classes)
    print(f'Collected {len(season_stages)} stages')


    stage_sessions = []
    if (path.exists('stage_sessions.dump')):
        stage_sessions = read_stage_sessions_cache()
    else:    
        stage_sessions = collect_stage_sessions(season_stages)
    print(f'Collected {len(stage_sessions)} sessions')

    session_entries = []
    if (path.exists('session_entries.dump')):
        session_entries = read_session_entries_cache()
    else:
        session_entries = collect_session_entries(stage_sessions)
    print(f'Collected {len(session_entries)} sessions entries')

    ssp_600_data = [
            record.session_entries for record in session_entries 
            if (record.stage_session.name == 'RACE' 
            or record.stage_session.name == 'Q')
            and record.stage_session.season_name in ['2019', '2018', '2017', '2016']
            and record.stage_session.season_class.name == 'AMATEUR1'
            and 'Полтава' in record.stage_session.season_stage.name
    ]
    
    values = []
    for entry in ssp_600_data:
        for sub in entry:
            if (sub.best_lap_time and sub.best_lap_time != '0.000' and sub.best_lap_time != '0' ):
                lap_time_destructured = list(sub.best_lap_time)
                if (lap_time_destructured[1] == '.'):
                    lap_time_destructured[1] = ':'
                if (lap_time_destructured[4] == ':'):
                    lap_time_destructured[4] = '.'
                if (not ":" in lap_time_destructured):
                    lap_time_destructured.insert(0, '00:')
                if (not "." in lap_time_destructured):    
                    lap_time_destructured.insert(len(lap_time_destructured) - 3, '.')
                assembled = ''.join(lap_time_destructured).replace(' ', '').strip()
                parsed_time = datetime.strptime(assembled, '%M:%S.%f')
                
                sub.best_lap_time = parsed_time.strftime('%M:%S.%f')[:-3]
                values.append(sub)


    values.sort(key=lambda x: (x.best_lap_time, x.best_lap_time if (x.best_lap_time) else -1))

    values.sort(key=lambda x: x.surname + '_' + x.name)

    personal_values = []
    for k, g in groupby(values, lambda x: x.surname + '_' + x.name):
        times = list(g)
        times.sort(key=lambda x: x.best_lap_time, reverse=False)
        if (times):
            personal_values.append(times[0])

    
    personal_values.sort(key=lambda x: (x.best_lap_time, x.best_lap_time if (x.best_lap_time) else -1))

    for index, personal_values in enumerate(personal_values):
        print(f'#{index+1} {personal_values.surname} {personal_values.name} {personal_values.best_lap_time}') 


mine()    