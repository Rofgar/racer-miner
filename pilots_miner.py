import requests
import os.path
import codecs
import json
from json import JSONDecoder, JSONEncoder
from os import path
from lxml import html
from lxml.cssselect import CSSSelector
from itertools import groupby
from datetime import datetime

class PilotMeta():
    def __init__(self, name, records_url):
        self.name = name
        self.records_url = records_url

    def __repr__(self):
        return self.name

class PilotBestTime():
    def __init__(self, name, track, session_type, lap_time, motorcycle, date):
        self.name = name
        self.track = track
        self.session_type = session_type
        self.lap_time = lap_time
        self.motorcycle = motorcycle
        self.date = date
    
    def __repr__(self):
        return f'{self.name} [{self.lap_time}]'

class ObjectEncoder(JSONEncoder):
    def default(self, o):
        return o.__dict__

class PilotBestTimeDecoder(JSONDecoder):
    def __init__(self):
        json.JSONDecoder.__init__(self, object_hook=self.dict_to_object)

    def dict_to_object(self, dictionary):
        return PilotBestTime(name=dictionary['name'], track=dictionary['track'], session_type=dictionary['session_type'], lap_time=dictionary['lap_time'], motorcycle=dictionary['motorcycle'], date=dictionary['date'])

def mine():
    pilots_response = ''
    if (path.exists("pilots.dump")):
        print('Using cached pilots dump')
        with codecs.open('pilots.dump', 'r', encoding='utf-8') as pilots_file:
            pilots_response = pilots_file.read()
    else:
        pilots_response = requests.get('http://racer.com.ua/ua/personslist/piloty')
        if (not pilots_response.ok):
            print('Could not retrieve pilots list')
            return
        pilots_response = pilots_response.text
        with codecs.open('pilots.dump', 'w', encoding='utf-8') as pilots_file:
            pilots_file.write(pilots_response)
    pilots_html = html.fromstring(pilots_response)

    pilot_metas = []
    for pilot in pilots_html.cssselect('a.piloty'):
        pilot_name = pilot.cssselect('h4.pilot-name')[0]
        for br in pilot_name.cssselect('br'):
            br.tail = ' ' + br.tail
            br.drop_tag()
        pilot_metas.append(PilotMeta(pilot_name.text_content(), pilot.get('href')))

    pilot_records = []
    if (path.exists('pilot_records.dump')):
        print('Using cached pilot records dump')
        with codecs.open('pilot_records.dump', 'rb') as pilot_records_file:
            pilot_records = json.loads(pilot_records_file.read(), cls=PilotBestTimeDecoder)
    else:
        for pilot in pilot_metas:
            pilot_records_response = requests.get(pilot.records_url)
            if (not pilot_records_response.ok):
                print(f'Could not retrieve {pilot} meta data')
                return
            pilot_records_html = html.fromstring(pilot_records_response.text)
            record_rows = pilot_records_html.cssselect('#tab3 table tbody tr')
            if (not record_rows):
                print(f'Missing data row for {pilot}')
                continue
            
            for record_row in record_rows:
                record_elements = [element.text_content().strip() for element in record_row.cssselect('td')]
                if (record_elements):
                    pilot_records.append(PilotBestTime(pilot.name, record_elements[0], record_elements[1], record_elements[2], record_elements[3], record_elements[4]))
                else: 
                    print(f'Missing data element for {pilot}')

        if (pilot_records):
            try:
                with codecs.open('pilot_records.dump', 'wb') as pilot_records_file:
                    pilot_records_file.write(json.dumps(pilot_records, cls=ObjectEncoder, ensure_ascii=False).encode('utf8'))
            except Exception as e:
                print('Failed to write records to cache: ' + str(e))
                os.remove('pilot_records.dump')
        else:
            print('Empty records will not be cached')

    chaika_times = [record for record in pilot_records if record.lap_time != '0.000']

    for record in chaika_times:
        lap_time_destructured = list(record.lap_time)
        if (lap_time_destructured[1] == '.'):
            lap_time_destructured[1] = ':'
        if (lap_time_destructured[4] == ':'):
            lap_time_destructured[4] = '.'
        if (not ":" in lap_time_destructured):
            lap_time_destructured.insert(0, '00:')
        if (not "." in lap_time_destructured):    
            lap_time_destructured.insert(len(lap_time_destructured) - 3, '.')
        parsed_time = datetime.strptime(''.join(lap_time_destructured).replace(' ', ''), '%M:%S.%f')
        record.lap_time = parsed_time.strftime('%M:%S.%f')[:-3]

    top = []
    chaika_times.sort(key=lambda x: x.name + '_' + x.track)

    for k, g in groupby(chaika_times, lambda x: x.name + '_' + x.track):
        times = list(g)
        times.sort(key=lambda x: x.lap_time, reverse=False)
        if (times):
            top.append(times[0])

    top.sort(key=lambda x: x.track)
    times_by_tracks = {}
    for k, g in groupby(top, lambda x: x.track):
        if times_by_tracks.get(k):
            times_by_tracks[k].append(g)
        else:
            times_by_tracks[k] = list(g)

    for track, times in times_by_tracks.items():
        print(f'Track {track}')
        times.sort(key=lambda x: x.lap_time)
        for index, record in enumerate(times):
            print(f'#{index+1} {record}')


mine()
